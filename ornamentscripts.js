var products = [{
	name:"Horse Bag",
	oldPrice: 1500.00,
	newPrice: 1399.99,
	image: "images/product/elephantsA.png",
	link: "OrnamentDetail.html"
 }, { 
	name:"Plane Bag",
	oldPrice: 3000.00,
	newPrice: 2500.00,
	image: "images/product/elephantsB.png",
	link: "OrnamentDetail.html"
 }, {
	name:"Van Bag",
	oldPrice: 15.00,
	newPrice: 12.99,
	image: "images/product/elephantsC.jpg",
	link: "OrnamentDetail.html"
 }, {
	name:"Oar Bag",
	oldPrice: 500.00,
	newPrice: 300.00,
	image: "images/product/elephantsD.png",
	link: "OrnamentDetail.html"
 }, {
	name:"Bow Bag",
	oldPrice: 450.00,
	newPrice: 250.00,	
	image: "images/product/elephantsE.png",
	link: "OrnamentDetail.html"
 }, {
	name:"Italian Bag",
	oldPrice: 1550.00,
	newPrice: 1199.99,
	image: "images/product/elephantsF.jpg",
	link: "OrnamentDetail.html"
 }, {
	name:"American Bag",
	oldPrice: 1600.00,
	newPrice: 1390.99,
	image: "images/product/elephantsG.jpg",
	link: "OrnamentDetail.html"
 }, {
	name:"Eagle Bag",
	oldPrice: 300.20,
	newPrice: 259.99,
	image: "images/product/elephantsH.png",
	link: "OrnamentDetail.html"
 }, {
	name:"Elephant Bag",
	oldPrice: 500.00,
	newPrice: 450.00,
	image: "images/product/elephantsI.png",
	link: "OrnamentDetail.html"
 }];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});