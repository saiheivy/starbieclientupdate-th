var products = [{
	name:"Black Shoes",
	oldPrice: 123.20,
	newPrice: 50.99,
	image: "images/product/blackshoesA.jpg",
	link: "ShoesDetail.html"
 }, { 
	name:"White Shoes",
	oldPrice: 100,
	newPrice: 64.99,
	image: "images/product/blackshoesB.png",
	link: "ShoesDetail.html"
 }, {
	name:"Pink Shoes",
	oldPrice: 123.20,
	newPrice: 91.99,
	image: "images/product/blackshoesC.png",
	link: "ShoesDetail.html"
 }, {
	name:"Grey Shoes",
	oldPrice: 123.20,
	newPrice: 43.99,
	image: "images/product/blackshoesD.png",
	link: "ShoesDetail.html"
 }, {
	name:"Yellow Shoes",
	oldPrice: 123.20,
	newPrice: 52.99,	
	image: "images/product/blackshoesE.jpg",
	link: "ShoesDetail.html"
 }, {
	name:"Green Shoes",
	oldPrice: 123.20,
	newPrice: 60.99,
	image: "images/product/blackshoesF.jpg",
	link: "ShoesDetail.html"
 }, {
	name:"Blue Shoes",
	oldPrice: 123.20,
	newPrice: 64.99,
	image: "images/product/blackshoesG.jpg",
	link: "ShoesDetail.html"
 }, {
	name:"Orange Shoes",
	oldPrice: 123.20,
	newPrice: 99.99,
	image: "images/product/blackshoesH.jpg",
	link: "ShoesDetail.html"
 }, {
	name:"Red Shoes",
	oldPrice: 123.20,
	newPrice: 33.99,
	image: "images/product/blackshoesI.jpg",
	link: "ShoesDetail.html"
 }];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});
